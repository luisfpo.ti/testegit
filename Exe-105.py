
def notas(*n, sit=False):
    """ # cria o docstrings da função (help)
    → Função para analisar notas e situações de varios alunos.
    :param n: uma ou mais notas dos alunos
    :param sit: valor opcional, indicando se deve ou não adicionar a situação.
    :return: dicionario com varias informações.
    """
    r = {}
    r['total'] = len(n)
    r['maior'] = max(n)
    r['menor'] = min(n)
    r['media'] = sum(n)/len(n)
    if sit: #So faz a validação se situação for True
        if r['media'] > 7:
            r['situacao'] = 'BOA'
        elif r['media'] >= 5:
            r['situacao'] = 'RAZOAVEL'
        else:
            r['situacao'] = 'RUIM'

    return r 

resp = notas(5.8, 6.3, 5, sit=True) 
print(resp)   
help(notas)#exibe o help da função