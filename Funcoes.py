def titulo(txt): #função, txt recebe o parametro passado (TESTE 1)
    print('-' * 30)
    print(txt)
    print('-' * 30)

titulo(' TESTE 1') #Chamando a função e passando o paramentro para txt
titulo(' Parametro 2')

def teste():
    print('Funcao sem Parametro')
    print('-' * 30)

def soma(a, b):
    s = a + b
    print(f'Resultado da soma: {s}')
    print('-' * 30)

soma(4, 5) 
soma(8, 9)   

a = int(input('Digite o valor A:'))
b = int(input('Digite o valor B:'))
print('-' * 30)
soma(a, b)


def contador(*num): #Desempacotamento, diversos valores e joga em uma variavel
    print(num)
    for v in num:
        print(f'{v}', end='')
    tam = len(num)  
    print(tam) 
    print('-'* 20)
contador(2, 1, 5, 4, 6)
contador(4, 2, 1)


valores = [7 ,3 , 4, 5, 0]
def dobra(lst):
    pos = 0
    while pos < len(lst):
        lst[pos]*=2
        pos += 1

dobra(valores)
print(valores)
print('#'*15)
############### 
a = 3
def numero(b):
    global a
    a + 6
    print(a)
numero(a)    
#######################

def somar(a=0, b=0, c=0): #Uso de Return
    s = a + b + c
    return s #Retorna o resultado da variavel s, pode ser tbm True e False

r1 = somar(3, 5, 3)
r2 = somar(5, 20, 5) 
print(f'A soma de r1 é {r1} de r2 é {r2} e de r3 é {somar(5, 3)}')  #Posso chamar tbm direto no print  