jogador = {}
partidas = []
time = []
while True:
    jogador.clear()
    partidas.clear()
    jogador['nome'] = str(input('Nome do Jogador: '))
    tot = int(input(f'Quantas partidas {jogador["nome"]} jogou? '))
    for c in range(0, tot):
        partidas.append(int(input(f' Quantos gols na partida {c+1}? ')))
    jogador['gols'] = partidas[:]
    jogador['total'] = sum(partidas)
    time.append(jogador.copy())
    resp = str(input('Deseja cadastrar outro jogador? [S/N]')).lower()
    while resp not in 'sn':
        print('Incorreto, digite S ou N !')
        resp = str(input('Deseja cadastrar outro jogador? [S/N]')).lower()
    if 'n' in resp:
        break       
print('-=' * 30)
print('cod ', end='')
for i in jogador.keys():
    print(f'  {i:<15}', end='')
print()
print('--' * 30)    
for k, v in enumerate(time):
    print(f' {k:3>}', end='')
    for d in v.values():
        print(f'  {str(d):<15} ', end='')
    print()    
print('-=' * 30)

while True:
    busca = int(input('Mostrar dados do Jogador? (999 stop)'))
    if busca == 999:
        break
    if busca >= len(time):
        print(f'ERRO!não existe jogador com codigo {busca}')
    else:
        print(f'Levantamente do jogador {time[busca]["nome"]}')
        for i, g in enumerate(time[busca]['gols']):
            print(f'No jogo {i+1} fez {g} gols.')
    print('-' * 30)        

   
