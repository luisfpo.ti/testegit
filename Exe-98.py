
from time import sleep
def contador(i, f, p):

    if p < 0:
        p *= -1

    if p == 0:
        p = 1 

    print(f'Contagem de {i} até {f} de {p} em {p}')
    print('--'* 20)
   

    if i < f:
        cont = i
        while cont <= f:
            print(f'{cont} ', end='')
            cont += p
            sleep(0.5)
        print('FIM')
    else:
        cont = i 
        while cont >= f:
            print(f'{cont} ', end='')
            cont -= p
            sleep(0.5)
        print('FIM')            

# Fim função

#contador(1, 10, 1)
#contador(10, 0, 1)

inicio = int(input('Digite o valor do inicio: '))
fim = int(input('Digite o valor final: '))
passo = int(input('Entre com o valor do passo: '))

contador(inicio, fim, passo)