c = ('\033[m',        # 0 sem cores
     '\033[0;30;41m', # 1 Vermelho
     '\033[0;30;42m', # 2 Verde
     '\033[0;30;44m' # 3 azul
);

def ajuda(com):
    help(com)

def titulo(msg, cor=0):
    tam = len(msg) + 4
    print(c[cor], end='')
    print('~' * tam)
    print(f'  {msg}')
    print('~' * tam)    
    print(c[0], end='')

comando = ''
while True:
    titulo('SISTEMA DE AJUDA PyHelp', 2)
    comando = str(input('Função ou biblioteca > '))    
    if comando.upper() == 'FIM':
        break
    else:
        ajuda(comando)
titulo('ATE LOGO!', 1)        