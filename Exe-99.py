valores = []
def analise(lst):
    tot = len(lst)
    pos = 0
    while pos < len(lst):
        print('Analisando os valores processados....')
        print(f'{lst} - Foram informados {tot} ao todo.')
        maior = max(lst)
        print(f'O maior valor informado foi {maior}')
        print('-'* 40)
        pos += 1
qtd = int(input('Quantos valores deseja informar? '))
print('-'* 40)
for i in range(1, qtd+1):
    valores.append(int(input(f'Informe o valor {i}: ')))

analise(valores)
